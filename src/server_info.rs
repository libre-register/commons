use std::fmt;

/// Represents a Libre Register server's information, which is returned
/// when a client queries the index.
#[derive(Deserialize, Serialize, Clone, PartialEq, Eq, Debug)]
pub struct LibreRegisterServer {
    /// The ID of the server type. By default this is
    /// `LIBRE_REGISTER_SERVER`, although, in a modified version, this
    /// may be changed to `LIBRE_REGISTER_SERVER_MY_AWESOME_SCHOOL` or
    /// suchlike.
    pub id: String,
    /// Should be a [semver](semver.org) version, which can be parsed by
    /// the client so it can check if it meets required versioning.
    pub version: String,
}

impl fmt::Display for LibreRegisterServer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.id, self.version)
    }
}

impl std::str::FromStr for LibreRegisterServer {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(' ');
        Ok(Self {
            id: split.next().ok_or(())?.to_string(),
            version: split.next().ok_or(())?.to_string(),
        })
    }
}
